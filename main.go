package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	pkg "github.com/PocketLoan/pocketloan-monitoring-service/monitor"
)

func main() {
	// r := mux.NewRouter()

	// Perform Health Checks Every Hour
	monitorHealth := pkg.Monitor{
		Email: "jesseokeya@gmail.com",
		Links: map[string]string{
			"pocketloan-api":    "https://pocketloan-api.herokuapp.com/health",
			"pocketloan-auth":   "https://pocketloan-auth.herokuapp.com/health",
			"pocketloan-notify": "https://pocketloan-notify.herokuapp.com/health",
		},
	}

	monitorHealth.Every(1*time.Hour, func() {
		monitorHealth.Do(20, func() {
			response := monitorHealth.Request("GET", "")
			verify := monitorHealth.Verify(response, `{"message":"application is healthy","status":200}`)
			if verify == false {
				message := "Health Checks Failed On PocketLoan Monitoring Service"
				monitorHealth.Alert(message)
			}
		})
	})

	// r.HandleFunc("/", HomeHandler)
	// http.Handle("/", r)

	// port := SetPort("3001")
	// log.Fatal(http.ListenAndServe(port, r))
}

// HomeHandler routes user to application home
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`{"message":"welcome to pocketloan monitoring service", "status": "200"}`))
}

// SetPort allows you to set the server port
func SetPort(p string) string {
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = p
	}
	fmt.Println("pocketloan payment service server running on port *" + port)
	return ":" + port
}
